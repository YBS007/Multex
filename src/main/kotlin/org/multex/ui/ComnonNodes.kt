/**
 * This file should contain any nodes that are common between multiple views
 * @author Itay Rabin
 */

package org.multex.ui

import com.jfoenix.controls.JFXButton
import javafx.event.EventTarget
import javafx.scene.Node
import javafx.scene.control.Label
import org.multex.ui.Styles.Companion.backButton
import org.multex.ui.Styles.Companion.backIcon
import org.multex.ui.Styles.Companion.icon
import org.multex.ui.Styles.Companion.mediumSmall
import tornadofx.*

//val backImg = Label().addClass(icon, backIcon, mediumSmall) //the image of the back button

fun EventTarget.backButton(text: String = "BACK", graphic: Node? = null, op: (JFXButton.() -> Unit) = {}) {
    mdbutton(text = text, graphic = graphic ?: Label().addClass(icon, backIcon, mediumSmall), op = op).addClass(backButton)
}

