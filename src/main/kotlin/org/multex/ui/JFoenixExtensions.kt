/**
 * A file that contains extension functions to support TornadoFX Type-safe builders with Material Design JFoenix controls
 * @author Itay Rabin
 */

package org.multex.ui

import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXCheckBox
import com.jfoenix.controls.JFXListView
import com.jfoenix.controls.JFXTextField
import javafx.beans.property.Property
import javafx.beans.value.ObservableValue
import javafx.collections.ObservableList
import javafx.event.EventTarget
import javafx.scene.Node
import javafx.scene.control.Button
import tornadofx.*

/**
 * use this instead of the normal button for a material design button
 */
fun EventTarget.mdbutton(text: String = "", graphic: Node? = null, op: (JFXButton.() -> Unit) = {}): Button {
    val button = JFXButton(text)
    if(graphic != null) button.graphic = graphic
    return opcr(this, button, op)
}

/**
 * use this instead of the normal checkbox for a material design checkbox
 */
fun EventTarget.mdcheckbox(text: String? = null, property: Property<Boolean>? = null, op: (JFXCheckBox.() -> Unit) = {}) = opcr(this, JFXCheckBox(text).apply {
    if(property != null) bind(property)
}, op)

/**
 * use this instead of the normal text field for a material design text field
 */
fun EventTarget.mdtextfield(value: String? = null, op: (JFXTextField.() -> Unit) = {}) = opcr(this, JFXTextField().apply { if(value != null) text = value }, op)

/**
 * use this instead of the normal text field for a material design text field
 * this function also binds the string property to it
 */
fun EventTarget.mdtextfield(property: ObservableValue<String>, op: (JFXTextField.() -> Unit) = {}) = mdtextfield().apply {
    bind(property)
    op.invoke(this)
}

fun <T> EventTarget.mdlistview(values: ObservableList<T>? = null, op: (JFXListView<T>.() -> Unit) = {}) = opcr(this, JFXListView<T>().apply {
    if(values != null) {
        if(values is SortedFilteredList<T>) values.bindTo(this)
        else items = values
    }
}, op)