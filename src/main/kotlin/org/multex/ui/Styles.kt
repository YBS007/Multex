package org.multex.ui

import javafx.geometry.Pos.CENTER
import javafx.geometry.Pos.TOP_LEFT
import javafx.scene.Cursor
import javafx.scene.paint.Color
import javafx.scene.paint.Color.*
import javafx.scene.text.FontPosture.ITALIC
import javafx.scene.text.FontWeight.BOLD
import org.multex.h
import org.multex.w
import tornadofx.*

class Styles : Stylesheet() {
    companion object {
        //colors
        val defaultColor: Color = LIGHTBLUE
        val pressColor = c("#75bed6")
        val defaultColorContrast: Color = c("#111111aa")
        //        val lightBackgroundColor: Color = c("#fafafa")

        //containers and areas
        val mainContainer by cssclass()
        val content by cssclass()
        val separatedArea by cssclass()
        val errorArea by cssclass()

        //buttons and such
        val mainActionButton by cssclass()
        val backButton by cssclass()

        //icon definitions
        val icon by cssclass()
        val small by cssclass()
        val mediumSmall by cssclass()
        val medium by cssclass()
        val large by cssclass()

        //icons
        val crossIcon by cssclass()
        val backIcon by cssclass()

        //text
        val heading by cssclass()
    }

    init {
        val contentArea = mixin {
            borderRadius += box(4.px)
            maxWidth = 400.w.px
            alignment = CENTER
            backgroundColor += WHITE
        }

        mainContainer {
            backgroundColor += WHITE
            prefWidth = 600.w.px
            prefHeight = 740.h.px
        }

        content {
            padding = box(30.px)
            alignment = CENTER
        }

        mainActionButton {
            backgroundColor += defaultColor
            backgroundRadius += box(5.px)
            focusTraversable = false
            prefWidth = infinity

            unsafe("-jfx-button-type", "RAISED")
        }

        backButton {
            padding = box(15.px)
            alignment = TOP_LEFT
            fontSize = 1.5.em
        }

        checkBox {
            unsafe("-jfx-checked-color", pressColor)

            focusTraversable = false
            and(selected) {
                mark {
                    backgroundColor += defaultColorContrast
                    borderColor += box(defaultColorContrast)
                }

            }
        }

        button {
            focusTraversable = false
        }

        errorArea {
            +contentArea
            backgroundColor += c(252, 222, 222)
            borderColor += box(c(210, 178, 178))
            padding = box(12.w.px)
            padding = box(12.w.px)
            alignment = CENTER
            prefWidth = 500.w.px

            icon {
                backgroundColor += c(153, 17, 17)
            }

            label {
                textFill = c(153, 17, 17)
            }
        }

        separatedArea {
            +contentArea
            alignment = CENTER
        }

        heading {
            alignment = CENTER
            fontSize = 2.em
            fontStyle = ITALIC
            fontWeight = BOLD
            textFill = BLACK
        }

        icon {
            minWidth = 16.px
            maxWidth = 16.px
            minHeight = 16.px
            maxHeight = 16.px
            backgroundColor += GRAY
            and(small) {
                minWidth = 12.px
                maxWidth = 12.px
                minHeight = 12.px
                maxHeight = 12.px
            }
            and(mediumSmall) {
                minWidth = 18.px
                maxWidth = 18.px
                minHeight = 18.px
                maxHeight = 18.px
            }
            and(medium) {
                minWidth = 24.px
                maxWidth = 24.px
                minHeight = 24.px
                maxHeight = 24.px
            }
            and(large) {
                minWidth = 48.px
                maxWidth = 48.px
                minHeight = 48.px
                maxHeight = 48.px
            }
        }

        crossIcon {
            shape = "M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"
            cursor = Cursor.HAND
        }

        backIcon {
            shape = "M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"
        }
    }

}