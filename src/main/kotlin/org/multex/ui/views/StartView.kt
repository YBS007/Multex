package org.multex.ui.views

import javafx.beans.property.SimpleBooleanProperty
import javafx.geometry.Pos
import javafx.geometry.Pos.CENTER
import org.multex.d
import org.multex.ui.Styles
import org.multex.ui.mdbutton
import org.multex.ui.mdcheckbox
import org.multex.w
import tornadofx.*

/**
 * The first view you see when you open the app - gives you a choice whether to use this computer as a client
 * or as a server
 *
 * @author Itay Rabin
 */
class StartView : View("Multex") {
    private val shouldRemember = SimpleBooleanProperty()

    override val root = borderpane {
        addClass(Styles.mainContainer)
        minWidth = 590.d
        minHeight = 465.d

        top {
            hbox {
                useMaxWidth = true
                alignment = Pos.TOP_RIGHT
                paddingAll = 10.d

                imageview("LayerMultex.png") {
                    fitWidth = 100.d
                    fitHeight = 100.d
                    useMaxWidth = true
                }
            }
        }

        center {
            vbox(50.w) {
                addClass(Styles.content)
                useMaxWidth = true
                style { fontSize = (1.5).em }

                vbox {
                    alignment = CENTER
                    label("Multex").addClass(Styles.heading)

                    label("Connecting People") {
                        style { fontSize = 1.5.em }
                    }
                }

                label("Choose This Computer's Role:")

                mdbutton {
                    addClass(Styles.mainActionButton)

                    text = "Use As Server"
                    action {
                        replaceWith(ServerLoginView::class, sizeToScene = true)
                    }
                }

                mdbutton {
                    addClass(Styles.mainActionButton)

                    text = "Use As Client"
                    setOnAction {
                        replaceWith(ClientConnectionView::class, sizeToScene = true)
                    }
                }


            }
        }
    }
}