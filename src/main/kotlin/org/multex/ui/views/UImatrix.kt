package org.multex.ui.views

import javafx.beans.property.SimpleBooleanProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.paint.Color
import org.multex.d
import org.multex.findClient
import org.multex.listeners.ClipboardListener
import org.multex.networking.Server
import org.multex.networking.payloads.ClientData
import org.multex.ui.Styles
import org.multex.ui.Styles.Companion.mainActionButton
import org.multex.ui.backButton
import org.multex.ui.mdbutton
import org.multex.ui.mdlistview
import tornadofx.*

class UImatrix: View("Order Devices") {

    //global variables deceleration
    private val devicesList: ObservableList<String> = FXCollections.observableArrayList<String>()
    private var tempList = mutableListOf<String>()
    private var tempList1 = mutableListOf<String>()
    private var selected: String? = "None"
    private val list = listOf("top left ", "top", "top right", "left center", "center", "center right", "left bottom", "bottom", "right bottom")
    private lateinit var grid: DataGrid<String>

    private val isStarted = SimpleBooleanProperty(false)

    override val root = borderpane {
        addClass(Styles.mainContainer)
        minWidth = 620.d
        minHeight = 600.d
        maxWidth = 620.d
        maxHeight = 600.d

        //dataGrid initialization list
        val names: ObservableList<String> = FXCollections.observableArrayList(list)

        top {
            hbox {
                //back botton
                backButton {
                    action {
                        replaceWith(ServerLoginView::class, sizeToScene = true)
                    }
                }

                //list for the devices
                mdlistview(devicesList) {
                    orientation = Orientation.HORIZONTAL
                    setPrefSize(500.d, 60.d)

                    //changing selected when user chose a device
                    onUserSelect(1) {
                        selected = selectedItem

                    }
                }
            }
        }

        center {
            vbox {
                alignment = Pos.CENTER

                label("Arrange Your Computers - Server Must Be In The Matrix") {
                    alignment = Pos.CENTER

                    style {
                        textFill = Color.RED
                        fontSize = 2.em
                    }
                }
                grid = datagrid(names) {
                    disableWhen { isStarted }
                    onUserSelect(1) {
                        if(selected != "None") //checking user selected something
                        {
                            val name = selectedItem
                            //changing the list string
                            if(tempList.contains(selectedItem)) //checking if need to be returned to device list
                            {
                                devicesList.add(selectedItem)
                            }
                            if(!devicesList.contains(selected)) {
                                names[names.indexOf(selected)] = tempList1[names.indexOf(selected)]
                            }
                            names[names.indexOf(name)] = selected // changing the selected spot to the selected item
                            devicesList.remove(selected)
                            selected = "None"
                        }
                        else if(!tempList1.contains(selectedItem)) {
                            selected = selectedItem
                        }
                    }
                }
            }
        }

        bottom {
            useMaxWidth = true
            hbox {
                useMaxWidth = true
                mdbutton("Start/Stop") {
                    addClass(mainActionButton)
                    minWidth = 100.d
                    useMaxWidth = true
                    hboxConstraints {
                        marginTopBottom(10.d)
                        marginLeftRight(10.d)
                    }
                    action {
                        //creating the user matrix
                        if(isStarted.value == false) {
                            val top = mutableListOf<ClientData?>()
                            val center = mutableListOf<ClientData?>()
                            val bottom = mutableListOf<ClientData?>()
                            var i = 0
                            var toAdd: String
                            for(name in names) {
                                toAdd = if(tempList1.contains(name)) "Empty" else name
                                when(i) {
                                    in 0..2 -> top.add(Server.clientList.findClient(toAdd))
                                    in 3..5 -> center.add(Server.clientList.findClient(toAdd))
                                    else -> bottom.add(Server.clientList.findClient(toAdd))
                                }
                                i += 1
                            }
                            Server.usersMatrix = listOf(top, center, bottom)
                            println(Server.usersMatrix)

                            //checking if server is in the matrix
                            var flag = false
                            for(l in Server.usersMatrix) {
                                for(c in l) {
                                    if(c?.name == "Server") flag = true
                                }
                            }
                            if(flag) {
                                Server.runListener()
                                ClipboardListener()
                                isStarted.value = true
                            }
                        }
                        else {
                            Server.stopListener()
                            isStarted.value = false
                        }

                    }
                }
            }

        }
    }

    override fun onUndock() {
        Server.stopListener()
        Server.clientList.remove(Server.server)
    }

    override fun onDock() {
        devicesList.clear()
        Server.clientList.add(Server.server)
        Server.clientList.forEach { client -> devicesList.add(client.name) }
        currentStage?.isResizable = false

        tempList.clear()
        tempList1.clear()

        grid.items.clear()
        grid.items.addAll(list)


        //temps for lists that change during run
        for(name in devicesList) tempList.add(name)
        for(item in list) tempList1.add(item)
    }
}