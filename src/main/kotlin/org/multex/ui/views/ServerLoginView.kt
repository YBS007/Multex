package org.multex.ui.views

import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Pos.CENTER
import javafx.geometry.Pos.CENTER_RIGHT
import javafx.scene.control.MultipleSelectionModel
import javafx.scene.paint.Color.RED
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import org.multex.d
import org.multex.networking.Helper
import org.multex.networking.Server
import org.multex.ui.Styles
import org.multex.ui.Styles.Companion.content
import org.multex.ui.Styles.Companion.mainActionButton
import org.multex.ui.backButton
import org.multex.ui.mdbutton
import org.multex.ui.mdlistview
import org.multex.w
import tornadofx.*

/**
 * @author Itay Rabin
 */

class ServerLoginView: View("Server Login") {

    override val root = borderpane {
        addClass(Styles.mainContainer)
        top {
            vbox {
                backButton {
                    action {
                        replaceWith(StartView::class, sizeToScene = true)
                    }
                }
                label("Connect Clients") {
                    addClass(Styles.heading)
                    useMaxWidth = true
                    alignment = CENTER
                }
            }
        }

        center {
            borderpane {
                addClass(content)

                top {
                    textflow {
                        text("Your IP Address Is ")
                        text(Helper.getLocalHostLANAddress().hostAddress) {
                            fill = RED
                            tooltip("Enter this IP to client computers")
                        }
                        style {
                            fontSize = 2.em
                            padding = box(5.px)
                        }

                    }
                }

                left {
                    vbox(70.w) {
                        paddingAll = 20
                        label("Connected Clients: ") {
                            useMaxWidth = true
                            alignment = CENTER_RIGHT
                            style {
                                fontSize = 1.25.em
                            }
                        }
                        spacer()
                        label("Connect All Clients Now") {
                            useMaxWidth = true
                            alignment = CENTER
                            style {
                                textFill = RED
                            }
                        }
                        spacer()
                    }
                }

                center {
                    mdlistview(Server.clientList).selectionModel = NoSelectionModel()
                }

                bottom {
                    mdbutton {
                        addClass(mainActionButton)
                        style { fontSize = 1.5.em }
                        alignment = CENTER
                        useMaxWidth = true
                        paddingAll = 10.d

                        text = "Continue"
                        action {
                            replaceWith(UImatrix::class, sizeToScene = true)
                        }
                    }
                }
            }
        }
    }

    override fun onUndock() {
        Server.stopAccepting()
    }


    init {
        launch(CommonPool) {
            Server.accept()
        }
    }
}

class NoSelectionModel<T>: MultipleSelectionModel<T>() {
    override fun getSelectedIndices(): ObservableList<Int> = FXCollections.emptyObservableList()
    override fun selectIndices(index: Int, vararg indices: Int) {}
    override fun isEmpty(): Boolean = true
    override fun selectNext() {}
    override fun selectLast() {}
    override fun isSelected(index: Int): Boolean = false
    override fun select(obj: T) {}
    override fun select(index: Int) {}
    override fun clearSelection() {}
    override fun clearSelection(index: Int) {}
    override fun selectAll() {}
    override fun getSelectedItems(): ObservableList<T> = FXCollections.emptyObservableList()
    override fun selectFirst() {}
    override fun clearAndSelect(index: Int) {}
    override fun selectPrevious() {}
}
