package org.multex.ui.views

import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Pos
import javafx.scene.control.SelectionMode
import javafx.scene.image.Image
import javafx.scene.paint.Color
import javafx.scene.text.FontPosture
import javafx.scene.text.FontWeight
import org.multex.d
import org.multex.findClient
import org.multex.h
import org.multex.networking.*
import org.multex.networking.Client.json
import org.multex.networking.payloads.*
import org.multex.ui.Styles.Companion.mainActionButton
import org.multex.ui.Styles.Companion.mainContainer
import org.multex.ui.mdbutton
import org.multex.ui.mdlistview
import org.multex.ui.mdtextfield
import org.multex.w
import tornadofx.*
import java.io.DataOutputStream
import java.io.File
import java.io.InputStream
import java.net.InetAddress
import java.net.ServerSocket

/**
 * @author Itay Rabin
 */
class FileTransferView: View("Send Files") {

    private var fileToSend: List<File?> = listOf()
    private val filesTextProperty = SimpleStringProperty("")

    private var pendingConnections = -1
    private var connectionsDone = 0

    override val root = vbox(10) {
        addClass(mainContainer)
        style {
            prefWidth = 450.w.px
            prefHeight = 800.h.px
        }

        label {
            text = "Choose Files To Send"
            isUnderline = true
            useMaxWidth = true
            style {
                alignment = Pos.CENTER
                fontSize = 2.em
                fontStyle = FontPosture.ITALIC
                fontWeight = FontWeight.BOLD
                textFill = Color.BLACK
            }
        }

        hbox(15) {
            useMaxWidth = true
            paddingAll = 10

            mdbutton {
                graphic = imageview(Image(resources.stream("/ic_insert_drive_file_black_24dp_1x.png")))
                setOnAction {
                    fileToSend = chooseFile("Choose Files To Send", arrayOf(), mode = FileChooserMode.Multi).observable()
                    filesTextProperty.value = if(fileToSend.isNotEmpty()) fileToSend.toString().substring(1, fileToSend.toString().length - 1) else ""
                }
            }

            mdbutton {
                graphic = imageview(Image(resources.stream("/ic_folder_open_black_24dp_1x.png")))
                setOnAction {
                    fileToSend = listOf(chooseDirectory("Choose Folder To Send")).observable()
                    filesTextProperty.value = if(fileToSend.isNotEmpty()) fileToSend.toString().substring(1, fileToSend.toString().length - 1) else ""
                }
            }

            mdtextfield {
                textProperty().bind(filesTextProperty)
                isDisable = true
                useMaxWidth = true
                prefWidth = 400.d

            }
        }

        val list = mdlistview(selectList()) {
            selectionModel.selectionMode = SelectionMode.MULTIPLE
            useMaxHeight = true
        }

        mdbutton {
            addClass(mainActionButton)
            useMaxWidth = true
            text = "SEND"

            vboxConstraints {
                marginLeftRight(10.d)
            }

            setOnAction {
                if(list.selectionModel.selectedItems.isNotEmpty() && fileToSend.isNotEmpty()) {
                    waitForFileRequests()

                    pendingConnections = list.selectionModel.selectedItems.size
                    val ip = Helper.getLocalHostLANAddress().hostAddress
                    for(c in list.selectionModel.selectedItems) {
                        if(Client.dOut != null) {
                            Client.dOut?.writeUTF(Client.json.stringify(Packet(ProtocolDefinitions.PREPARE_FILE_TRANSFER, FileNotificationData(ip, c.name))))
                        }
                        else {
                            Server.sendFileNotification(Packet(ProtocolDefinitions.PREPARE_FILE_TRANSFER, FileNotificationData(ip, c.name)))
                        }
                    }
                }
            }
        }
    }

    private fun selectList(): ObservableList<ClientData> = when {
        Server.clientList.isNotEmpty() -> {
            val temp = Server.clientList
            if(Client.dIn == null) temp.remove(temp.findClient("Server"))
            temp
        }
        Client.dOut != null -> {
            Client.requestConnectedList()
        }

        else -> FXCollections.emptyObservableList()
    }

    private fun waitForFileRequests() {
        val sendSocket = ServerSocket(ProtocolDefinitions.FILE_SEND_PORT, 0, InetAddress.getByName("0.0.0.0"))

        Thread {
            try {
                val target = pendingConnections
                while(pendingConnections > 0) {
                    val sock = sendSocket.accept()
                    pendingConnections--

                    Thread {
                        val cache = ArrayList<File>(fileToSend)
                        println("cache = ${cache}")
                        val out = DataOutputStream(sock?.getOutputStream())
                        println("ClipboardListenerClient.waitForFileRequests")
                        out.writeAndFlush(json.stringify(Packet(ProtocolDefinitions.FILE_DATA, RootData(cache.size))))

                        for(f in cache) sendFileToClient(out, f)
                        connectionsDone++

                        if(connectionsDone == target) sendSocket.close()
                    }.start()
                }

                sendSocket.close()
            }
            catch(e: Exception) {
            }
        }.start()
    }

    private fun sendFileToClient(out: DataOutputStream, f: File) {
        println("ClipboardListenerClient.sendFileToClient")
        if(f.isDirectory) {
            val fs = f.listFiles()
            out.writeAndFlush(json.stringify(Packet(ProtocolDefinitions.FILE_DATA, DirectoryData(f.name, fs.size))))
            for(f1 in fs) sendFileToClient(out, f1)
        }
        else {
            out.writeAndFlush(json.stringify(Packet(ProtocolDefinitions.FILE_DATA, SingleFileData(f.name))))
            out.writeLong(f.length())
            out.flush()
            f.inputStream().writeToDataOutputStream(out)

        }
    }
}

fun DataOutputStream.writeAndFlush(str: String) {
    this.writeUTF(str)
    this.flush()
}

fun InputStream.writeToDataOutputStream(out: DataOutputStream, bufferSize: Int = 4096) {
    var count: Int
    val buffer = ByteArray(bufferSize) // or 4096, or more

    count = this.read(buffer)
    while(count > 0) {
        println("count = ${count}")
        out.write(buffer, 0, count)
        out.flush()
        count = this.read(buffer)
    }

    close()
}
