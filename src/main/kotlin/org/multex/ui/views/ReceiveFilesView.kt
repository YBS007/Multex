package org.multex.ui.views

import javafx.application.Platform
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.ProgressBar
import javafx.stage.Stage
import org.multex.d
import org.multex.findClient
import org.multex.networking.Client
import org.multex.networking.Packet
import org.multex.networking.ProtocolDefinitions
import org.multex.networking.Server
import org.multex.networking.payloads.DirectoryData
import org.multex.networking.payloads.RootData
import org.multex.networking.payloads.SingleFileData
import org.multex.ui.Styles
import org.multex.ui.Styles.Companion.mainContainer
import tornadofx.*
import java.io.DataInputStream
import java.io.File
import java.net.InetAddress
import java.net.Socket
import java.nio.file.Paths

/**
 * @author Itay Rabin
 */
class ReceiveFilesView: View("File Transfer") {

    private val nameProp = SimpleStringProperty("")
    private val fileProp = SimpleStringProperty("")
    lateinit var pb: ProgressBar

    override val root = vbox(10) {
        paddingAll = 20
        println("1")
        addClass(mainContainer)
        prefWidth = 400.d
        prefHeight = 200.d

        label {
            useMaxWidth = true
            alignment = Pos.CENTER
            textProperty().bindBidirectional(nameProp)
            addClass(Styles.heading)
        }

        label(fileProp)

        pb = progressbar {
            useMaxWidth
        }
    }

    private fun startRec() {
        println("2")
        nameProp.value = "Receiving Files"
        val f = chooseDirectory("Choose Destination")
        if(f == null) {
            //TODO: not true
            if(Server.clientList.isNotEmpty()) Server.clientList.findClient(name)?.dOut?.writeUTF(Server.json.stringify(Packet(ProtocolDefinitions.CANCEL_FILE_TRANSFER)))
            else Client.dOut?.writeUTF(Client.json.stringify(Packet(ProtocolDefinitions.CANCEL_FILE_TRANSFER)))
            currentStage?.close()
            return
        }

        Thread { getExternalFiles(f) }.start()
    }

    private fun getExternalFiles(f: File) {
        println("Client.getExternalFiles")
        println("clipboardFilesIp = $ip")

        val sock = Socket(InetAddress.getByName(ip.replace("\\", "").replace("/", "")), ProtocolDefinitions.FILE_SEND_PORT)
        println("connected")
        val root = f.absolutePath

        getFile(DataInputStream(sock.getInputStream()), root)

        Platform.runLater {
            currentStage?.close()
        }
    }

    private fun getFile(inS: DataInputStream, dir: String) {
        val str = inS.readUTF()
        println("read")
        val data = Client.json.parse<Packet>(str)
        println("Client.getFile")

        when {
            data.payload is RootData -> repeat(data.payload.num) {
                getFile(inS, dir)
            }

            data.payload is DirectoryData -> {
                val f = Paths.get(dir, data.payload.name).toFile()
                f.mkdir()

                repeat(data.payload.itemsInside) {
                    getFile(inS, f.absolutePath)
                }
            }

            data.payload is SingleFileData -> {
                val f = Paths.get(dir, data.payload.name).toFile()

                Platform.runLater {
                    fileProp.value = "Getting file: " + data.payload.name
                }

                val os = f.outputStream()
                var count: Int
                val buffer = ByteArray(4096)
                val fileSize = inS.readLong()
                println(fileSize)
                var readAlready = 0
//                count = inS.read(buffer)
//                readAlready += count
                 do {
                     count = inS.read(buffer, 0, if(fileSize - readAlready < 4096) (fileSize - readAlready).toInt() else 4096)
                     if(count > 0) readAlready += count
                    Platform.runLater {
                        pb.progress = readAlready.d / fileSize
                    }
                    os.write(buffer, 0, count)

                }while(readAlready < fileSize)

                os.close()
            }
        }
    }

    companion object {
        private val newStage = Stage()
        private lateinit var ip: String
        private lateinit var name: String

        fun startRecv(ownerIP: String, ownerName: String) {
            ip = ownerIP
            name = ownerName
            try {
                val newScene = Scene(find(ReceiveFilesView::class).root)
                FX.applyStylesheetsTo(newScene)

                newStage.title = "File Transfer"
                newStage.scene = newScene
                newStage.setOnShowing {
                    find<ReceiveFilesView>().startRec()
                }
            }
            catch(e: Exception) {
            }
            finally {
                newStage.show()
                newStage.toFront()
            }

        }
    }
}
