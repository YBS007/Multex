package org.multex.ui.views

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos.CENTER
import javafx.scene.control.Button
import javafx.scene.layout.HBox
import javafx.scene.media.Media
import javafx.scene.media.MediaPlayer
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import org.multex.d
import org.multex.h
import org.multex.listeners.ClipboardListenerClient
import org.multex.networking.Client
import org.multex.networking.ProtocolDefinitions.CLIENT_CANT_CONNECT
import org.multex.networking.ProtocolDefinitions.CONNECTION_FAILED_NAME_TAKEN
import org.multex.networking.ProtocolDefinitions.CONNECTION_FAILED_NO_MORE_ROOM
import org.multex.networking.ProtocolDefinitions.CONNECTION_OK
import org.multex.ui.Styles
import org.multex.ui.Styles.Companion.crossIcon
import org.multex.ui.Styles.Companion.errorArea
import org.multex.ui.Styles.Companion.mainContainer
import org.multex.ui.Styles.Companion.small
import org.multex.ui.backButton
import org.multex.ui.mdbutton
import org.multex.ui.mdtextfield
import org.multex.w
import tornadofx.*
import java.io.File
import java.net.InetAddress

/**
 * @author Itay Rabin
 */
class ClientConnectionView : View("Connect To Server") {
    /**
     * these 3 values are used to get information from the user and validate it.
     */
    private val connectionData = ViewModel()
    private val screenName = connectionData.bind { SimpleStringProperty() }
    private val serverIP = connectionData.bind { SimpleStringProperty() }

    /**
     * these 2 values are used to determine the state of the view
     */
    private val isConnectionSuccessful = SimpleBooleanProperty(false)
    private val buttonText = stringBinding(isConnectionSuccessful) {
        return@stringBinding when(value) {
            true -> "Connected"
            false -> "Connect"
        }
    }

    private val errorMessagePlaceHolder by cssid()

    override val root = vbox {
        addClass(mainContainer)
        style {
            prefWidth = 800.w.px
            prefHeight = 450.h.px
        }

        backButton {
            action {
                Client.disconnect()
                replaceWith(StartView::class, sizeToScene = true)
            }
        }
        //in this node we put the error dialog, so we give it an Id
        hbox { alignment = CENTER }.setId(errorMessagePlaceHolder)

        form {
            disableWhen { isConnectionSuccessful } //disable the form then login is successful
            addClass(Styles.content)
            fieldset("Connection Info:") {
                spacing = 30.d
                alignment = CENTER

                style {
                    paddingAll = 10
                    fontSize = 1.5.em
                }
                field("Screen Name: ") {
                    mdtextfield(screenName) {
                        text = InetAddress.getLocalHost().hostName
                        required()
                    }
                }

                field("Server IP Address: ") {
                    mdtextfield(serverIP).validator(ValidationTrigger.None) { return@validator validateIP(it) }
                }

                mdbutton {
                    addClass(Styles.mainActionButton)

                    textProperty().bind(buttonText)
                    isDefaultButton = true

                    action { connect() }
                }
            }
        }
    }

    /**
     * validation of an IP address
     */
    private fun ValidationContext.validateIP(it: String?): ValidationMessage? {
        return try {
            val isOk = it != null && it.split('.').size == 4 && it.split('.').map { it.toInt() }.none { it !in 0..255 }

            if(isOk) return null
            error("Not A Valid IP Address")
        } //an Exception will be thrown if toInt() fails
        catch(e: Exception) {
            error("Not A Valid IP Address")
        }
    }

    /**
     * this function is called when the connect button is pressed.
     * it checks if the data is valid, and if so tries to connect
     */
    private fun Button.connect() {
        if(connectionData.commit()) {
            var result = ""
            runAsyncWithProgress {
                result = Client.attemptConnection(screenName.value, serverIP.value)
            } ui {
                when(result) {
                    CONNECTION_FAILED_NO_MORE_ROOM -> onError("All 8 Computers Are Connected, Can't Connect More")
                    CONNECTION_FAILED_NAME_TAKEN -> onError("Computer Name Already used")
                    CLIENT_CANT_CONNECT -> onError("Couldn't find server - are you sure you entered the right IP address?")
                    CONNECTION_OK -> onSuccess()
                }
            }
        }
    }

    private fun onError(errorMessage: String) {
        isConnectionSuccessful.value = false
        root.select<HBox>(errorMessagePlaceHolder).replaceChildren {
            hbox {
                addClass(errorArea)
                alignment = CENTER
                label(errorMessage)
                spacer()
                button {
                    addClass(crossIcon, Styles.icon, small)
                    action(this@hbox::removeFromParent)
                }
            }
        }
    }

    private fun onSuccess() {
        val musicFile = "src/main/resources/chime_bell_ding.wav"

        val sound = Media(File(musicFile).toURI().toString())
        val mediaPlayer = MediaPlayer(sound)
        mediaPlayer.play()

        isConnectionSuccessful.value = true
        launch(CommonPool) {
            ClipboardListenerClient()
            Client.listen()
        }
    }

    override fun onUndock() {
        Client.disconnect()
        isConnectionSuccessful.value = false
    }
}
