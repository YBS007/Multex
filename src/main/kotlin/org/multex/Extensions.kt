package org.multex

import javafx.geometry.Rectangle2D
import javafx.stage.Screen
import kotlinx.coroutines.experimental.javafx.JavaFx
import org.multex.networking.Server
import org.multex.networking.payloads.ClientData
import java.awt.Rectangle
import java.net.Socket

val Int.d get() = this.toDouble() //yes I am this lazy, don't judge me
val Int.w: Double get() = (this / 1920.0) * Screen.getPrimary().visualBounds.width
val Int.h: Double get() = (this / 1080.0) * Screen.getPrimary().visualBounds.height

fun List<ClientData>.findClient(name: String): ClientData? {
    return this.find {
        it.name == name
    }
}

/**
 * an extension function for adding a client to the clientList.
 * NOTICE THE run(UI) BLOCK: this is done because changing this list must be done from the UI thread
 */
suspend fun ClientData.add() = kotlinx.coroutines.experimental.run(JavaFx) {
    Server.clientList.add(this)
}

/**
 * an extension function for the disconnection of a client - removing him for the clientList.
 * here also, this must be done from the UI thread
 */
suspend fun ClientData.disconnect() = kotlinx.coroutines.experimental.run(JavaFx) {
    if (!Server.clientList.remove(this)) System.err.println("tried removing this $this, but it was not in the connected list")
}

/**
 * the handleClient function only has access to the socket of the client - so this
 * function finds the client based on the given socket
 *
 * @param socket a socket to search for a client by
 * @return the client if found, null otherwise
 */
fun List<ClientData>.findClient(socket: Socket): ClientData? {
    /* NOTICE the return syntax - the last line of each block is the thing that will be returned
    so in this case the this.first for the try block and null for the catch block.
    */
    return try {
        this.first { it.socket == socket }
    } catch (e: Exception) {
        System.err.println("searched for client, but it was not in the list")
        null
    }
}

fun List<List<ClientData?>>.findClientPos(clientInFocus: ClientData): Pair<Int, Int> {
    this.forEachIndexed { i, it ->
        it.forEachIndexed { j, client ->
            if (client == clientInFocus)
                return Pair(i, j)
        }
    }

    throw IllegalStateException("clientInFocus is not in the matrix")
}

fun Rectangle2D.toRectangle(): Rectangle? = Rectangle(minX.toInt(), minY.toInt(), width.toInt(), height.toInt())