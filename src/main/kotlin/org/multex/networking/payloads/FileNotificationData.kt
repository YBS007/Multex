package org.multex.networking.payloads

import kotlinx.serialization.Serializable

@Serializable data class FileNotificationData(val ownerIP: String, val recp: String)