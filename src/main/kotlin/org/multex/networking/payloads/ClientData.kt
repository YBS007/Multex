package org.multex.networking.payloads

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import java.io.DataInputStream
import java.io.DataOutputStream
import java.net.Socket

@Serializable data class ClientData(val name: String, val noScreens: Int, val OS: String, @Transient var socket: Socket? = null, @Transient var dIn: DataInputStream? = null, @Transient var dOut: DataOutputStream? = null) {
    override fun toString() = name
}
