package org.multex.networking.payloads



import kotlinx.serialization.Serializable


@Serializable data class KeyboardData(val rawCode : Int)