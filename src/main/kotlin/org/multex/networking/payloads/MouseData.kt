package org.multex.networking.payloads

import kotlinx.serialization.Serializable

/**
 * this file defines Mouse Events that can be sent to a client
 * @author Itay Rabin
 */

/**
 * base class of a mouse event. sealed means only the classes in this file can inherit from it, so we don't have to have
 * an else in a when statement
 */
@Serializable sealed class MouseData

/**
 * an event that represents the movement of a mouse
 *
 * @param delay the time between this event and the one before it. not currently used, because adding delays makes the mouse movement laggy
 * @param x the x position of the mouse, in percent of the width of the screen
 * @param y the y position of the mouse, in percent of the height of the screen
 */
@Serializable data class MouseMovedEvent(val delay: Long, var x: Double, var y: Double) : MouseData()

/**
 * an event that represents the press of a mouse button
 *
 * @param delay the time between this event and the one before it. not currently used, because adding delays makes the mouse movement laggy
 * @param button an int representing the index of the button
 */
@Serializable data class MousePressedEvent(val delay: Long, val button: Int): MouseData()

/**
 * an event that represents the release of a mouse button
 *
 * @param delay the time between this event and the one before it. not currently used, because adding delays makes the mouse movement laggy
 * @param button an int representing the index of the button
 */
@Serializable data class MouseReleasedEvent(val delay: Long, val button: Int): MouseData()

/**
 * an event that represents the press of a mouse button
 *
 * @param delay the time between this event and the one before it. not currently used, because adding delays makes the mouse movement laggy
 * @param amount where and how much was the scroll wheel scrolled
 */
@Serializable data class MouseWheelEvent(val delay: Long, val amount: Int): MouseData()
