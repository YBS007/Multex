package org.multex.networking.payloads

import kotlinx.serialization.Serializable

/**
 * @author Itay Rabin
 */

@Serializable sealed class FileData

@Serializable data class RootData(val num: Int): FileData()

@Serializable data class DirectoryData(val name: String, val itemsInside: Int): FileData()

@Serializable data class SingleFileData(val name: String): FileData()