package org.multex.networking.payloads

import kotlinx.serialization.Serializable


@Serializable data class ClipboardData(val data : String , val code : Int)
