package org.multex.networking

/**
 * a file containing all the things that are in common between Server and Client
 *
 *
 */
object ProtocolDefinitions {
    const val PORT = 1948
    const val FILE_SEND_PORT = 20018
    const val MPS = 200.0 //Messages Per Second

    const val CONNECTION_OK = "101"
    const val CONNECTION_FAILED_NAME_TAKEN = "102"
    const val CONNECTION_FAILED_NO_MORE_ROOM = "103"

    const val CLIENT_CONNECTION_REQUEST = "111"
    const val CLIENT_DISCONNECT_REQUEST = "112"

    const val CLIENT_CANT_CONNECT = "client_cant_find_server"

    const val MOUSE_DATA = "201"

    const val LOST_FOCUS = "701"

    const val BORDER_THRESHOLD = 0.998

    const val KEYBOARD_PRESS = "301"
    const val KEYBOARD_RELEASE = "302"

    const val CLIPBOARD_COPY_FROM_SERVER = "401"
    const val CLIPBOARD_COPY_FROM_CLIENT = "402"
    const val STRING_DATA = 1

    const val PREPARE_FILE_TRANSFER = "410"
    const val CANCEL_FILE_TRANSFER = "412"
    const val FILE_DATA = "413"

    const val REQUEST_CLIENT_LIST = "500"
    const val RESPOND_CLIENT_LIST = "501"
}