package org.multex.networking

import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable



@Serializable data class Packet(val code: String, @Optional val payload: Any? = null)