package org.multex.networking

//import org.multex.networking.ProtocolDefinitions.NOTIFY_FILES_COPIED_FROM_SERVER
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.stage.Screen
import kotlinx.serialization.json.JSON
import org.multex.listeners.ClipboardHelper
import org.multex.networking.ProtocolDefinitions.CLIENT_CANT_CONNECT
import org.multex.networking.ProtocolDefinitions.CLIENT_CONNECTION_REQUEST
import org.multex.networking.ProtocolDefinitions.CLIENT_DISCONNECT_REQUEST
import org.multex.networking.ProtocolDefinitions.CLIPBOARD_COPY_FROM_SERVER
import org.multex.networking.ProtocolDefinitions.CONNECTION_OK
import org.multex.networking.ProtocolDefinitions.KEYBOARD_PRESS
import org.multex.networking.ProtocolDefinitions.KEYBOARD_RELEASE
import org.multex.networking.ProtocolDefinitions.LOST_FOCUS
import org.multex.networking.ProtocolDefinitions.MOUSE_DATA
import org.multex.networking.ProtocolDefinitions.PORT
import org.multex.networking.ProtocolDefinitions.PREPARE_FILE_TRANSFER
import org.multex.networking.ProtocolDefinitions.REQUEST_CLIENT_LIST
import org.multex.networking.ProtocolDefinitions.RESPOND_CLIENT_LIST
import org.multex.networking.ProtocolDefinitions.STRING_DATA
import org.multex.networking.payloads.*
import org.multex.ui.views.ReceiveFilesView
import java.awt.GraphicsEnvironment
import java.awt.HeadlessException
import java.awt.Robot
import java.awt.event.InputEvent
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.net.InetAddress
import java.net.Socket

object Client {
    val json = JSON.indented //we can change this to whatever type we want - indented maybe a bit slower, but it's easier to debug

    /**
     * exposing the dIn var so it can be used in all functions
     */
    var dIn: DataInputStream? = null

    /**
     * exposing the dOut var so it can be used in all functions
     */
    var dOut: DataOutputStream? = null

    /**
     * this var is used to stop the listening thread when a disconnect happens
     */
    private var listen = false

    private val clientListCopy: ObservableList<ClientData> = FXCollections.observableList(mutableListOf())

    private val transparentScreen = TransparentScreen()

    fun attemptConnection(computerName: String, serverIp: String): String {
        try {
            //creating the socket and the input and output stream
            val socket = Socket(InetAddress.getByName(serverIp), PORT)
            socket.tcpNoDelay = true
            dIn = DataInputStream(socket.getInputStream())
            dOut = DataOutputStream(socket.getOutputStream())

            val operatingSystem = getOperatingSystem()

            //getting the number of screens
            val noScreens = getNoScreens()

            //generate Json string
            val jsonString = json.stringify(Packet(CLIENT_CONNECTION_REQUEST, payload = ClientData(computerName, noScreens, operatingSystem)))

            //sending the string to the server
            dOut?.writeUTF(jsonString)
            dOut?.flush()

            //returning the server response
            val rez = dIn?.readUTF()
            //if the connection was successful start listening
            listen = rez == CONNECTION_OK
            return rez.toString()
        }
        catch(e: IOException) {
            System.err.println("error connecting to server $e")
            return CLIENT_CANT_CONNECT
        }
    }

    /**
     * this function returns the amount of screens connected to the device
     *
     * @return the amount of screens
     */
    private fun getNoScreens(): Int {
        //return try syntax - the last line of each block is the one to be returned
        return try {
            // Get local graphics environment
            val env = GraphicsEnvironment.getLocalGraphicsEnvironment()

            // Returns an array of all of the screen GraphicsDevice objects.
            val devices = env.screenDevices

            devices.size
        }
        catch(e: HeadlessException) {
            // We'll get here if no screen devices was found.
            e.printStackTrace()
            -1
        }
    }

    /**
     * gets the OS
     *
     * @return the name of the OS
     */
    private fun getOperatingSystem(): String {
        val operatingSystem = System.getProperty("os.name").toLowerCase()

        return when {
            operatingSystem.startsWith("windows") -> "windows"
            operatingSystem.startsWith("linux") -> "linux"
            operatingSystem.startsWith("mac") -> "mac"
            operatingSystem.startsWith("android") -> "android"
            else -> "Unknown"
        }
    }

    /**
     * this function sends a disconnection message and stops listening
     */
    fun disconnect() {
        try {
            dOut?.writeUTF(json.stringify(Packet(CLIENT_DISCONNECT_REQUEST)))
            dOut?.flush()
        }
        catch(e: Exception) {
            System.err.println("UNABLE TO DISCONNECT, SERVER SHUT DOWN?")
        }
        listen = false
    }

    /**
     * this function listens to packets from the server and handles them accordingly
     */
    suspend fun listen() {
        val robot = Robot() //the robot that will be used to emulate mouse and keyboard events
        var startTime: Long //used to measure time - delays can suck balls and some blocks should be as fast as possible (handling mouse events for example)

        transparentScreen.show()

        while(listen) {
            if(dIn == null) continue
            try {
                startTime = System.currentTimeMillis()

                //read the packet and time it - if the server send many in a row but readTime is large means there is an issue with the server
                val str = dIn?.readUTF()
                val readTime = System.currentTimeMillis() - startTime

                //parsing the Json string - shouldn't really take long, but whatever
                val packet = json.parse<Packet>(str!!)

                //if we get large delay, it is either because we lagged or because there was nothing to receive
                //                if (readTime + parseTime > 50) {
                //                    System.err.println("large time. no events or lag?\n$str\n")
                //                }
                @Suppress("UNCHECKED_CAST") //this line disables annoying warnings.
                when(packet.code) {
                    MOUSE_DATA -> doMouseEvents(robot, packet.payload as List<MouseData>)
                    KEYBOARD_RELEASE -> doKeyboardRelease(packet.payload as KeyboardData, robot)
                    KEYBOARD_PRESS -> doKeyboardPress(packet.payload as KeyboardData, robot)
                    LOST_FOCUS -> transparentScreen.show()
                    CLIPBOARD_COPY_FROM_SERVER -> addToClipboard(packet.payload as ClipboardData)
                    RESPOND_CLIENT_LIST -> clientListCopy.addAll(packet.payload as ArrayList<ClientData>)
                    PREPARE_FILE_TRANSFER -> Platform.runLater { ReceiveFilesView.startRecv((packet.payload as FileNotificationData).ownerIP, "???") }
                }
            }
            catch(e: IOException) {
                //this block is reached if the server is shutdown, so we don't need to listen anymore
                listen = false
            }
        }
    }


    private fun addToClipboard(data: ClipboardData) {
        if(data.code == STRING_DATA) {
            ClipboardHelper().addToClipboard(data.data)
        }
    }

    private suspend fun doKeyboardRelease(data: KeyboardData, robot: Robot) {
        robot.keyRelease(data.rawCode)
    }

    private suspend fun doKeyboardPress(data: KeyboardData, robot: Robot) {
        robot.keyPress(data.rawCode)
    }

    /**
     * this functions emulates mouse events sent from the server
     */
    private suspend fun doMouseEvents(robot: Robot, mouseEvents: List<MouseData>) {
        if(transparentScreen.isShowing()) transparentScreen.hide()

        mouseEvents.forEach {
            when(it) {
                is MouseMovedEvent -> {
                    //MouseMoveEvent gives it's coordinates in percent of screen, so we need to multiply it back
                    //in the client resolution
                    /* val nx = (Screen.getPrimary().visualBounds.width * it.x).toInt()
                     val ny = (Screen.getPrimary().visualBounds.height * it.y).toInt()
                     val (x, y) = MouseInfo.getPointerInfo().location
                     val d = Point(nx, ny)
                     d.translate(-x, -y)
                     robot.mouseMove(x + d.x / 4, y + d.y / 4)
                     robot.mouseMove(x + d.x / 2, y + d.y / 2)
                     robot.mouseMove(x + d.x / (4.0 / 3).toInt(), y + d.y / (4.0 / 3).toInt())
                     robot.mouseMove(x + d.x, y + d.y)*/
                    robot.mouseMove((Screen.getPrimary().bounds.width * it.x).toInt(), (Screen.getPrimary().bounds.height * it.y).toInt())
                }

                is MousePressedEvent -> {
                    robot.mousePress(InputEvent.getMaskForButton(it.button.makeButtonCorrect()))
                }

                is MouseReleasedEvent -> {
                    robot.mouseRelease(InputEvent.getMaskForButton(it.button.makeButtonCorrect()))
                }

                is MouseWheelEvent -> {
                    robot.mouseWheel(it.amount)
                }
            }
        }
    }

    fun requestConnectedList(): ObservableList<ClientData> {
        println("req")
        dOut?.writeUTF(json.stringify(Packet(REQUEST_CLIENT_LIST)))
        clientListCopy.clear()
        return clientListCopy
    }
}

/**
 * JNativeHook and Robot have different numbers for right-click and wheel-click.
 * this might actually be platform specific - on windows-windows right click and wheel-click need to be swapped
 */
private fun Int.makeButtonCorrect(): Int {
    if(this == 1) return 1
    if(this == 2) return 3
    /*if (this == 3)*/ return 2
}
