package org.multex.networking

import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.stage.Screen
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import kotlinx.serialization.json.JSON
import kotlinx.serialization.json.JSON.Companion.parse
import org.jnativehook.GlobalScreen
import org.multex.add
import org.multex.disconnect
import org.multex.findClient
import org.multex.findClientPos
import org.multex.listeners.*
import org.multex.networking.ProtocolDefinitions.BORDER_THRESHOLD
import org.multex.networking.ProtocolDefinitions.CLIENT_CONNECTION_REQUEST
import org.multex.networking.ProtocolDefinitions.CLIENT_DISCONNECT_REQUEST
import org.multex.networking.ProtocolDefinitions.CLIPBOARD_COPY_FROM_CLIENT
import org.multex.networking.ProtocolDefinitions.CONNECTION_FAILED_NAME_TAKEN
import org.multex.networking.ProtocolDefinitions.CONNECTION_FAILED_NO_MORE_ROOM
import org.multex.networking.ProtocolDefinitions.CONNECTION_OK
import org.multex.networking.ProtocolDefinitions.LOST_FOCUS
import org.multex.networking.ProtocolDefinitions.MOUSE_DATA
import org.multex.networking.ProtocolDefinitions.PORT
import org.multex.networking.ProtocolDefinitions.PREPARE_FILE_TRANSFER
import org.multex.networking.ProtocolDefinitions.REQUEST_CLIENT_LIST
import org.multex.networking.ProtocolDefinitions.RESPOND_CLIENT_LIST
import org.multex.networking.Server.Border.*
import org.multex.networking.payloads.ClientData
import org.multex.networking.payloads.ClipboardData
import org.multex.networking.payloads.FileNotificationData
import org.multex.networking.payloads.MouseMovedEvent
import org.multex.ui.views.ReceiveFilesView
import java.awt.Robot
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.net.InetAddress
import java.net.ServerSocket
import java.net.Socket
import java.util.logging.Level
import java.util.logging.Logger
import kotlinx.coroutines.experimental.javafx.JavaFx as UI


object Server {
    /**
     * this is the json object used to parse from string and write to string
     */
    val json = JSON.indented

    /**
     * this is a listener sent to JNativeHook to get mouse events
     */
    private val mouseListener = MouseListener()
    private val keyboardListner = KeyboardListener()
    private var listenForNewConnections = false

    /**
     * an observable list of all clients connected.
     * it is observable so the connected clients list in the UI will update automatically when the list is changed
     * changing this list MUST be done from the UI thread, so in a run(UI) {..} block.
     */
    val clientList: ObservableList<ClientData> = FXCollections.observableArrayList<ClientData>()

    private val transparentScreen = TransparentScreen()

    var usersMatrix: List<MutableList<ClientData?>> = mutableListOf()
    val server = ClientData("Server", -1, "")
    private val robot = Robot() //needed to set mouse pos when switching back to the server

    var clientInFocus: ClientData? = server
        set(value) {
            if(System.getProperty("os.name").toLowerCase().startsWith("linux")) {
                val x = ExtendedX11.INSTANCE

                val display = x.XOpenDisplay(null)
                val window = x.XDefaultRootWindow(display)
            }
            if(value == null) return

            if(field == server) {
                //TODO: SERVER LOST FOCUS - DO STUFF ABOUT IT
                transparentScreen.show()
            }


            if(value == server) {
                //focus on Server
                transparentScreen.hide()
                //keyboardListner.linuxUngrabKeyboard()
                keyboardListner.stop()


            }else {
                sendLostFocus(field!!)
                keyboardListner.start()
                //keyboardListner.linuxGrabKeyboard()
            }

            field = value
        }

    fun accept() {
        val serverSocket = ServerSocket(PORT, 0, InetAddress.getByName("0.0.0.0"))

        listenForNewConnections = true

        while(listenForNewConnections) {
            try {
                //accepting the client
                val socket = serverSocket.accept()
                socket.tcpNoDelay = true
                println("Connected ${socket.inetAddress}")
                launch(CommonPool) {
                    handleClient(socket) //starting a thread to handle client
                }
            }
            catch(e: IOException) {
                System.out.println("error") //handling error
            }
        }
    }

    fun stopAccepting() {
        listenForNewConnections = false
    }

    /**
     * this function registers the JNativeHook and the mouse listener
     */
    fun runListener() {
        //these commands are used to turn off JNativeHook logging - we don't want it to make our output messy
        val logger = Logger.getLogger(GlobalScreen::class.java.`package`.name)
        logger.level = Level.OFF
        logger.useParentHandlers = false

        GlobalScreen.setEventDispatcher(VoidDispatchService())
        GlobalScreen.registerNativeHook()

        GlobalScreen.addNativeMouseListener(mouseListener)
        GlobalScreen.addNativeMouseMotionListener(mouseListener)
        GlobalScreen.addNativeMouseWheelListener(mouseListener)

        //turn on sending events to clients on a new thread - CommonPool means it is run on a background thread
        launch(CommonPool) { recordAndSendMouseEvents() }
    }

    /**
     * this function unregisters the JNativeHook and the mouse listener
     */
    fun stopListener() {
        if(GlobalScreen.isNativeHookRegistered()) {
            GlobalScreen.unregisterNativeHook()
        }
    }

    private suspend fun handleClient(socket: Socket) {
        //creating the input and output stream
        val dIn = DataInputStream(socket.getInputStream())
        val dOut = DataOutputStream(socket.getOutputStream())

        loop@ while(true) {
            try {
                val message = dIn.readUTF()
                //creating the jsonObject
                val data = parse<Packet>(message)

                //getting the code
                when(data.code) { //checking for code
                    CLIENT_CONNECTION_REQUEST -> {
                        val returnCode = createClient(data, socket, dIn, dOut) //trying to create the client
                        //sending the code to the client (success or fail
                        dOut.writeUTF(returnCode)
                        dOut.flush()
                        //if the login was not successful we don't need to keep the thread alive
                        if(returnCode != CONNECTION_OK) break@loop
                    }

                    CLIENT_DISCONNECT_REQUEST -> {
                        clientList.findClient(socket)?.disconnect()
                        break@loop
                    }

                    CLIPBOARD_COPY_FROM_CLIENT -> {
                        val c = data.payload as ClipboardData
                        ClipboardHelper().addToClipboard(c.data)
                        sendToAllClients(c, clientList.findClient(socket))
                    }

                    REQUEST_CLIENT_LIST -> {
                        val temp = clientList.toMutableList()
                        temp.remove(temp.findClient(socket))
                        dOut.writeUTF(json.stringify(Packet(RESPOND_CLIENT_LIST, temp)))
                        dOut.flush()
                    }

                    PREPARE_FILE_TRANSFER -> {
                        if((data.payload as FileNotificationData).recp == "Server") {
                            Platform.runLater {
                                ReceiveFilesView.startRecv(data.payload.ownerIP, clientList.findClient(socket)?.name!!)
                            }
                        }
                        else sendFileNotification(data)
                    }
                }
            }
            catch(e: IOException) {
                System.err.println("dIn.readUTF() failed, disconnecting client at socket $socket")
                clientList.findClient(socket)?.disconnect()
                break
            }
        }
    }

    fun sendFileNotification(p: Packet) {
        clientList.findClient((p.payload as FileNotificationData).recp)?.dOut?.writeUTF(json.stringify(p))
    }

    private suspend fun createClient(packet: Packet, socket: Socket, dIn: DataInputStream, dOut: DataOutputStream): String {

        if(clientList.size == 8) return CONNECTION_FAILED_NO_MORE_ROOM  //check if the list is full

        val client = packet.payload as ClientData

        if(nameExists(client.name)) //checking if name already in use
            return CONNECTION_FAILED_NAME_TAKEN

        //adding to the list
        client.socket = socket
        client.dIn = dIn
        client.dOut = dOut
        client.add()

        return CONNECTION_OK
    }

    /**
     * this function checks if a name exists in the clientList
     *
     * @param name the name to check
     * @return true if the name exists, false otherwise
     */
    private fun nameExists(name: String): Boolean = clientList.any { it.name == name }

    /**
     * this function handles sending clients
     */
    private suspend fun recordAndSendMouseEvents() {
        //        var startTime: Long
        //        var delayTime: Long
        //        startTime = System.currentTimeMillis()

        while(GlobalScreen.isNativeHookRegistered()) {
            //startTime = System.currentTimeMillis()

            if(mouseListener.eventList.size > 0) {
                val moveEvent = mouseListener.eventList.toList().findLast { it is MouseMovedEvent } as MouseMovedEvent?
                if(moveEvent != null) {
                    val border = getBorder(moveEvent.x, moveEvent.y)
                    val newClient = changeFocus(border)
                    if(newClient != null && newClient != clientInFocus) handleBorderChange(border, moveEvent, newClient)
                }

                if(clientInFocus != server) {
                    val jsonString = json.stringify(Packet(MOUSE_DATA, mouseListener.eventList.toList()))

                    try {
                        clientInFocus!!.dOut?.writeUTF(jsonString)
                        clientInFocus!!.dOut?.flush()
                    }
                    catch(e: Exception) {
                    }
                }

                mouseListener.eventList.clear() //delete the eventList, so only new events will be sent each time
            }

            //                println("System.currentTimeMillis() - realTime = ${System.currentTimeMillis() - realTime}")
            //                if (System.currentTimeMillis() - startTime > 20) {
            //                    println("System.currentTimeMillis() - startTime = ${System.currentTimeMillis() - startTime}")
            //                    startTime = System.currentTimeMillis()
            //                }
        }
    }

    private fun handleBorderChange(border: Border, event: MouseMovedEvent, newClient: ClientData) {
        mouseListener.eventList.clear()
        when(border) {
            TOP -> event.y = BORDER_THRESHOLD - 0.01
            TOP_LEFT -> {
                event.y = BORDER_THRESHOLD - 0.001
                event.x = BORDER_THRESHOLD - 0.001
            }
            TOP_RIGHT -> {
                event.y = BORDER_THRESHOLD - 0.001
                event.x = 1 - BORDER_THRESHOLD + 0.001
            }
            LEFT -> event.x = BORDER_THRESHOLD - 0.001
            RIGHT -> event.x = 1 - BORDER_THRESHOLD + 0.001
            BOTTOM_LEFT -> {
                event.x = BORDER_THRESHOLD - 0.001
                event.y = 1 - BORDER_THRESHOLD + 0.001
            }
            BOTTOM -> event.y = 1 - BORDER_THRESHOLD + 0.001
            BOTTOM_RIGHT -> {
                event.x = 1 - BORDER_THRESHOLD + 0.001
                event.y = 1 - BORDER_THRESHOLD + 0.001
            }
            NONE -> {
            }
        }
        mouseListener.eventList.add(event)

        val bounds = if(newClient == server) Screen.getPrimary().bounds else Screen.getPrimary().visualBounds
        robot.mouseMove(((event.x * bounds.width) + bounds.minX).toInt(),
                ((event.y * bounds.height) + bounds.minY).toInt())

        clientInFocus = newClient
    }

    private fun getBorder(x: Double, y: Double): Border = when {
        x > BORDER_THRESHOLD && y > BORDER_THRESHOLD -> BOTTOM_RIGHT
        x > BORDER_THRESHOLD && y < 1 - BORDER_THRESHOLD -> TOP_RIGHT
        x > BORDER_THRESHOLD -> RIGHT
        x < 1 - BORDER_THRESHOLD && y > BORDER_THRESHOLD -> BOTTOM_LEFT
        x < 1 - BORDER_THRESHOLD && y < 1 - BORDER_THRESHOLD -> TOP_LEFT
        x < 1 - BORDER_THRESHOLD -> LEFT
        y > BORDER_THRESHOLD -> BOTTOM
        y < 1 - BORDER_THRESHOLD -> TOP
        else -> NONE
    }

    private fun changeFocus(border: Border): ClientData? {
        val (y, x) = usersMatrix.findClientPos(clientInFocus!!)
        return when(border) {
            TOP -> if(y != 0) usersMatrix[y - 1][x] else clientInFocus
            TOP_LEFT -> when {
                y != 0 && x != 0 -> usersMatrix[y - 1][x - 1]
                x == 0 && y != 0 -> usersMatrix[y - 1][x]
                y == 0 && x != 0 -> usersMatrix[y][x - 1]
                else -> clientInFocus
            }
            TOP_RIGHT -> when {
                y != 0 && x != 2 -> usersMatrix[y - 1][x + 1]
                x == 2 && y != 0 -> usersMatrix[y - 1][x]
                y == 0 && x != 2 -> usersMatrix[y][x + 1]
                else -> clientInFocus

            }
            LEFT -> if(x != 0) usersMatrix[y][x - 1] else clientInFocus
            RIGHT -> if(x != 2) usersMatrix[y][x + 1] else clientInFocus
            BOTTOM_LEFT -> when {
                y != 2 && x != 0 -> usersMatrix[y + 1][x - 1]
                y != 2 && x == 0 -> usersMatrix[y + 1][x]
                y == 2 && x != 0 -> usersMatrix[y][x - 1]
                else -> clientInFocus
            }
            BOTTOM -> if(y != 2) usersMatrix[y + 1][x] else clientInFocus
            BOTTOM_RIGHT -> when {
                y != 2 && x != 2 -> usersMatrix[y + 1][x + 1]
                y != 2 && x == 2 -> usersMatrix[y + 1][x]
                y == 2 && x != 2 -> usersMatrix[y][x + 1]
                else -> clientInFocus

            }
            NONE -> clientInFocus
        }
    }

    private fun sendToAllClients(clipboardData: ClipboardData, except: ClientData? = null)
    {
        val jsonString = json.stringify(Packet(ProtocolDefinitions.CLIPBOARD_COPY_FROM_SERVER, payload = clipboardData))

        for(l in Server.usersMatrix) {
            for(c in l) {
                if(c != null && c != except) {
                    c.dOut?.writeUTF(jsonString)
                    c.dOut?.flush()
                }
            }
        }
    }

    private fun sendLostFocus(client: ClientData) {
        val packetToSend = Packet(LOST_FOCUS)
        client.dOut?.writeUTF(json.stringify(packetToSend))
        client.dOut?.flush()
    }

    enum class Border {
        TOP_LEFT, TOP, TOP_RIGHT, LEFT, NONE, RIGHT, BOTTOM_LEFT, BOTTOM, BOTTOM_RIGHT
    }
}