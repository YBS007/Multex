package org.multex.networking

import com.sun.jna.NativeLong
import com.sun.jna.platform.unix.X11
import javafx.stage.Screen
import org.multex.listeners.ExtendedX11
import org.multex.toRectangle
import java.awt.Color
import java.awt.Point
import java.awt.Rectangle
import java.awt.Toolkit
import java.awt.image.BufferedImage
import javax.swing.JWindow

/**
 * @author Itay Rabin
 */
class TransparentScreen {
    private val window: JWindow = JWindow()

    private var blocking = false
    private val useX11 = System.getProperty("os.name").toLowerCase().startsWith("linux")

    init {
        window.bounds = Rectangle(Screen.getPrimary().visualBounds.toRectangle())
        window.background = Color(0, 0, 0, 1)
        window.isAlwaysOnTop = true

        val cursorImg = BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB)
        val blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, Point(0, 0), "blank cursor")
        window.cursor = blankCursor
    }

    fun show() {
        window.isVisible = true
        if(useX11) {
            blocking = true
            Thread {
                val x = ExtendedX11.INSTANCE

                val display = x.XOpenDisplay(null)
                val window = x.XDefaultRootWindow(display)

                while(blocking) {
                    x.XGrabPointer(display,
                            window,
                            false,
                            X11.PointerMotionMask,
                            X11.GrabModeSync,
                            X11.GrabModeAsync,
                            X11.Window.None,
                            X11.Cursor.None,
                            NativeLong(X11.CurrentTime.toLong()))
                }
                x.XUngrabPointer(display, NativeLong(X11.CurrentTime.toLong()))
                x.XCloseDisplay(display)

                this@TransparentScreen.window.isVisible = false

            }.start()
        }
    }

    fun hide() {
        if(!useX11) window.isVisible = false

        blocking = false
    }

    fun isShowing(): Boolean = window.isVisible
}