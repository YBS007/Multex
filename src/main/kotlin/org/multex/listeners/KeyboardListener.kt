package org.multex.listeners

import com.sun.jna.NativeLong
import com.sun.jna.platform.unix.X11
import kotlinx.serialization.json.JSON
import org.jnativehook.GlobalScreen
import org.jnativehook.NativeHookException
import org.jnativehook.NativeInputEvent
import org.jnativehook.keyboard.NativeKeyEvent
import org.jnativehook.keyboard.NativeKeyListener
import org.multex.networking.Packet
import org.multex.networking.ProtocolDefinitions
import org.multex.networking.Server.clientInFocus
import org.multex.networking.payloads.KeyboardData
import java.awt.event.KeyEvent
import java.util.*
import java.util.concurrent.AbstractExecutorService
import java.util.concurrent.TimeUnit
import java.util.logging.Level
import java.util.logging.Logger

/**
 * @author Yair Ben Simon
 */

class KeyboardListener @Throws(NativeHookException::class)
constructor() : NativeKeyListener {
    private inner class VoidDispatchService : AbstractExecutorService() {
        private var running = false

        init {
            running = true
        }

        override fun shutdown() {
            running = false
        }

        override fun shutdownNow(): List<Runnable> {
            running = false
            return ArrayList(0)
        }

        override fun isShutdown(): Boolean {
            return !running
        }

        override fun isTerminated(): Boolean {
            return !running
        }

        @Throws(InterruptedException::class)
        override fun awaitTermination(timeout: Long, unit: TimeUnit): Boolean {
            return true
        }

        override fun execute(r: Runnable) {
            r.run()
        }
    }

    fun start() {
        // Create custom logger and level.
        val logger = Logger.getLogger(GlobalScreen::class.java.`package`.name)
        logger.level = Level.WARNING

        GlobalScreen.setEventDispatcher(VoidDispatchService())
        //GlobalScreen.registerNativeHook()

        GlobalScreen.addNativeKeyListener(this)
    }

    fun stop() {
        GlobalScreen.removeNativeKeyListener(this)
    }

    private val json = JSON.indented

    override fun nativeKeyPressed(e: NativeKeyEvent) {

         println(e.rawCode)
        try{


                val f = NativeInputEvent::class.java.getDeclaredField("reserved")
                f.isAccessible = true
                f.setShort(e, 0x01.toShort())




            val jsonString =  json.stringify(Packet(
                    ProtocolDefinitions.KEYBOARD_PRESS,
                    payload = KeyboardData(realRawCode(e.rawCode))
            ))

            clientInFocus?.dOut?.writeUTF(jsonString)
            clientInFocus?.dOut?.flush()

            } catch (ex: Exception) {
                print("[ !! ]\n")
                ex.printStackTrace()
            }

    }

    override fun nativeKeyReleased(e: NativeKeyEvent) {
            try {
                val f = NativeInputEvent::class.java.getDeclaredField("reserved")
                f.isAccessible = true
                f.setShort(e, 0x01.toShort())




                val jsonString =  json.stringify(Packet(
                        ProtocolDefinitions.KEYBOARD_RELEASE,
                         payload = KeyboardData(realRawCode(e.rawCode))
                ))

                clientInFocus?.dOut?.writeUTF(jsonString)
                clientInFocus?.dOut?.flush()


            } catch (ex: Exception) {
                print("[ !! ]\n")
                ex.printStackTrace()
            }
        }

    private fun realRawCode( rawCode : Int) : Int
    {
        val currentOS = System.getProperty("os.name").toLowerCase()
        return when(currentOS.startsWith("linux") && clientInFocus?.OS == "Windows") {
            true ->  linuxToWindowsKeyCode(rawCode)
            false -> windowsToWindowsKeyCode(rawCode)
        }
    }

    private fun linuxToWindowsKeyCode(rawCode: Int) : Int {
        return when(rawCode) {
            in 97..123-> rawCode-32
            65456 -> KeyEvent.VK_NUMPAD0
            65457 -> KeyEvent.VK_NUMPAD1
            65458 -> KeyEvent.VK_NUMPAD2
            65459 -> KeyEvent.VK_NUMPAD3
            65460 -> KeyEvent.VK_NUMPAD4
            65461 -> KeyEvent.VK_NUMPAD5
            65462 -> KeyEvent.VK_NUMPAD6
            65463 -> KeyEvent.VK_NUMPAD7
            65464 -> KeyEvent.VK_NUMPAD8
            65465 -> KeyEvent.VK_NUMPAD9
            65407 -> KeyEvent.VK_NUM_LOCK
            65454 -> KeyEvent.VK_PERIOD
            65421 -> KeyEvent.VK_ENTER
            65453 -> KeyEvent.VK_SUBTRACT
            65451 -> KeyEvent.VK_ADD
            65450 -> KeyEvent.VK_MULTIPLY
            65455 -> KeyEvent.VK_DIVIDE
            65505 -> KeyEvent.VK_SHIFT
            65506 -> KeyEvent.VK_SHIFT
            65514 -> KeyEvent.VK_ALT_GRAPH
            65507 -> KeyEvent.VK_CONTROL
            65508 -> KeyEvent.VK_CONTROL
            65513 -> KeyEvent.VK_ALT
            65509 -> KeyEvent.VK_CAPS_LOCK
            65289 -> KeyEvent.VK_TAB
            65361 -> KeyEvent.VK_LEFT
            65362 -> KeyEvent.VK_UP
            65363 -> KeyEvent.VK_RIGHT
            65364 -> KeyEvent.VK_DOWN
            65360 -> KeyEvent.VK_HOME
            65367 -> KeyEvent.VK_END
            65365 -> KeyEvent.VK_PAGE_UP
            65366 -> KeyEvent.VK_PAGE_DOWN
            65307 -> KeyEvent.VK_ESCAPE
            65470 -> KeyEvent.VK_F1
            65471 -> KeyEvent.VK_F2
            65472 -> KeyEvent.VK_F3
            65473 -> KeyEvent.VK_F4
            65474 -> KeyEvent.VK_F5
            65475 -> KeyEvent.VK_F6
            65476 -> KeyEvent.VK_F7
            65477 -> KeyEvent.VK_F
            65478 -> KeyEvent.VK_F9
            65479 -> KeyEvent.VK_F10
            65480 -> KeyEvent.VK_F11
            65481 -> KeyEvent.VK_F12
            65379 -> KeyEvent.VK_INSERT
            65535 -> KeyEvent.VK_DELETE
            65288 -> KeyEvent.VK_BACK_SPACE
            60 -> KeyEvent.VK_BACK_SLASH
            65293 -> KeyEvent.VK_ENTER
            96 -> KeyEvent.VK_BACK_QUOTE
            else->rawCode
        }
    }

    private fun windowsToWindowsKeyCode(rawCode: Int) : Int{
        return when(rawCode) {
            164 -> KeyEvent.VK_ALT
            165 -> KeyEvent.VK_ALT_GRAPH
            13 -> KeyEvent.VK_ENTER
            219 -> KeyEvent.VK_OPEN_BRACKET
            221 -> KeyEvent.VK_CLOSE_BRACKET
            161 -> KeyEvent.VK_SHIFT
            160 -> KeyEvent.VK_SHIFT
            162 -> KeyEvent.VK_CONTROL
            163 ->KeyEvent.VK_CONTROL
            45 -> KeyEvent.VK_INSERT
            46 ->KeyEvent.VK_DELETE
            186->KeyEvent.VK_SEMICOLON
            188->KeyEvent.VK_COMMA
            190->KeyEvent.VK_PERIOD
            220->KeyEvent.VK_BACK_SLASH
            226->KeyEvent.VK_BACK_SLASH
            191->KeyEvent.VK_SLASH
            91->KeyEvent.VK_WINDOWS
            else -> rawCode
        }

    }



    override fun nativeKeyTyped(e: NativeKeyEvent) { /* Unimplemented */
    }


    private val useX11 = System.getProperty("os.name").toLowerCase().startsWith("linux")
    private var blocking = false


    fun linuxGrabKeyboard()
    {
        if(useX11) {
            Thread {
                val x = ExtendedX11.INSTANCE

                val display = x.XOpenDisplay(null)
                val window = x.XDefaultRootWindow(display)
                blocking = true

                while(blocking) {
                    x.XGrabKeyboard(display,window, false, X11.GrabModeSync,
                            X11.GrabModeAsync, NativeLong(X11.CurrentTime.toLong()))
                }
                x.XUngrabKeyboard(display, NativeLong(X11.CurrentTime.toLong()))
                x.XCloseDisplay(display)
                x.XAllowEvents(display,X11.GrabModeSync,NativeLong(X11.CurrentTime.toLong()))
            }.start()
        }
    }

    fun linuxUngrabKeyboard()
    {
        if(useX11) {
            blocking = false
        }
    }


    companion object {

        @Throws(NativeHookException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            val keyboardListner = KeyboardListener()
            keyboardListner.start()
            GlobalScreen.registerNativeHook()

            GlobalScreen.addNativeKeyListener(keyboardListner)
            keyboardListner.start()
        }
    }
}