package org.multex.listeners;

import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.platform.unix.X11;
import com.sun.org.apache.xpath.internal.operations.Bool;

public interface ExtendedX11 extends X11 {

    ExtendedX11 INSTANCE = Native.loadLibrary("X11", ExtendedX11.class);

    int XGrabPointer(Display display,
                     X11.Window grab_window,
                     boolean owner_events,
                     int event_mask,
                     int pointer_mode,
                     int keyboard_mode,
                     Window confine_to,
                     Cursor cursor,
                     NativeLong time);

    int XUngrabPointer(Display display,
                       NativeLong time);

    int XGrabKeyboard(Display display, Window grab_window, boolean owner_events, int pointer_mode, int keyboard_mode, NativeLong time);

    int XAllowEvents(Display display, int event_mode,NativeLong time);
    int XUngrabKeyboard(Display display, NativeLong time);
}
