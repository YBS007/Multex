@file:Suppress("UNCHECKED_CAST")

package org.multex.listeners

import kotlinx.serialization.json.JSON
import org.multex.networking.Client
import org.multex.networking.Packet
import org.multex.networking.ProtocolDefinitions
import org.multex.networking.payloads.ClipboardData
import java.awt.Toolkit
import java.awt.datatransfer.*
/**
 * @author Yair Ben Simon
 */

class ClipboardListenerClient : ClipboardOwner {
    var json = JSON.indented

    init {
        println("In in the constructor")
        val clipboard = Toolkit.getDefaultToolkit().systemClipboard

        val lis = FlavorListener {
            try {
                val trans = clipboard.getContents(this)
                if (if (trans != null) trans.isDataFlavorSupported(DataFlavor.stringFlavor) else false) {
                    val tempText = trans.getTransferData(DataFlavor.stringFlavor) as String
                    System.out.println(tempText)
                    sendToServer(tempText, ProtocolDefinitions.STRING_DATA)

                }

                else {
                    println("not text")
                }
                clipboard.setContents(trans, this)
            } catch (e: Exception) {
                println("Fuck Fuck Fuck Fuck" + e)
            }
        }
        clipboard.addFlavorListener(lis)
    }


    override fun lostOwnership(clipboard: Clipboard, contents: Transferable) {
    }

    private fun sendToServer(data : String, code : Int) {
        val jsonString = json.stringify(Packet(ProtocolDefinitions.CLIPBOARD_COPY_FROM_CLIENT, payload = ClipboardData(data, code)))
        Client.dOut?.writeUTF(jsonString)
        Client.dOut?.flush()
    }
}