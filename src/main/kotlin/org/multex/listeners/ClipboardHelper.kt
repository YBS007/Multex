package org.multex.listeners

import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.ClipboardOwner
import java.awt.datatransfer.StringSelection
import java.awt.datatransfer.Transferable
import java.awt.Toolkit.getDefaultToolkit
import javafx.scene.input.Clipboard.getSystemClipboard
import java.awt.Toolkit

/**
 * @author Yair Ben Simon
 */

class ClipboardHelper : ClipboardOwner {
    override fun lostOwnership(clipboard: Clipboard?, contents: Transferable?) {
       println("replaced")
    }

    fun addToClipboard(data : String)
    {
        val clipboard = Toolkit.getDefaultToolkit().systemClipboard
        val contents = StringSelection(data)
        clipboard.setContents(contents, this)
    }
}

