@file:Suppress("UNCHECKED_CAST")

package org.multex.listeners

import kotlinx.serialization.json.JSON
import org.multex.networking.Packet
import org.multex.networking.ProtocolDefinitions
import org.multex.networking.Server
import org.multex.networking.payloads.ClipboardData
import java.awt.Toolkit
import java.awt.datatransfer.*

/**
 * @author Yair Ben Simon
 */

class ClipboardListener : ClipboardOwner {
    private val json = JSON.indented

    init {
        val clipboard = Toolkit.getDefaultToolkit().systemClipboard
        val lis = FlavorListener {
            try {
                val trans = clipboard.getContents(this)

                if (if (trans != null) trans.isDataFlavorSupported(DataFlavor.stringFlavor) else false) {
                    val tempText = trans.getTransferData(DataFlavor.stringFlavor) as String
                    System.out.println(tempText)
                    sendToAllClients(tempText,ProtocolDefinitions.STRING_DATA)
                }
                else {
                    println("not text")
                }
                clipboard.setContents(trans, this)
            } catch (e: Exception) {
                println("Fuck Fuck Fuck Fuck"+e)
            }
        }
        clipboard.addFlavorListener(lis)
    }

    override fun lostOwnership(clipboard: Clipboard, contents: Transferable) {
        println("Clipboard contents replaced")
    }

    private fun sendToAllClients(data: String, code: Int) {
        val jsonString =  json.stringify(Packet(ProtocolDefinitions.CLIPBOARD_COPY_FROM_SERVER, payload = ClipboardData(data,code)))

        Server.usersMatrix.forEach {
            it.forEach {
                it?.dOut?.writeUTF(jsonString)
                it?.dOut?.flush()
            }
        }
    }

    companion object {

        @JvmStatic
        fun main(args : Array<String>) {
            ClipboardListener()
            while (true);
        }
    }
}



