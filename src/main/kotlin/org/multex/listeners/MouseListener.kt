package org.multex.listeners

import javafx.geometry.Rectangle2D
import javafx.stage.Screen
import org.jnativehook.NativeInputEvent
import org.jnativehook.mouse.NativeMouseEvent
import org.jnativehook.mouse.NativeMouseInputListener
import org.jnativehook.mouse.NativeMouseWheelEvent
import org.jnativehook.mouse.NativeMouseWheelListener
import org.multex.networking.Server
import org.multex.networking.payloads.*
import kotlin.math.max
import kotlin.math.min

/**
 * a listener for all mouse events.
 * after registering it, mouse events will be sent here.
 * TODO: at some point these events will have to be consumed
 *
 * @author Itay Rabin
 */
class MouseListener: NativeMouseInputListener, NativeMouseWheelListener {
    /**
     * in windows and macOS, we keep the cursor in the transparent window by consuming
     * mouse movements that get out of it.
     * since that doesn't work on linux, we won't do it.
     */
    private val consumeWhenBad = System.getProperty("os.name").toLowerCase().startsWith("linux").not()

    /**
     * a list of all the events that happened after the last server tick
     */
    val eventList = mutableListOf<MouseData>()

    /**
     * for efficiency reasons, we don't want to allocate these variables in the function stack
     * over and over again, so we define them outside the nativeMouseMoved function.
     */
    private var x = 0.0
    private var y = 0.0
    private var vb: Rectangle2D = Screen.getPrimary().visualBounds

    /**
     * used to calculate the delay between 2 events. not sure if really needed, but why remove it?
     */
    private var lastEventTime: Long = System.currentTimeMillis()

    /**
     * this function gets called when the wheel is scrolled
     * it adds a MouseWheelEvent to the eventList
     *
     * @see MouseWheelEvent
     */
    override fun nativeMouseWheelMoved(event: NativeMouseWheelEvent?) {
        if(event == null) return

        eventList.add(MouseWheelEvent(if(eventList.size == 0) 0 else System.currentTimeMillis() - lastEventTime, event.wheelRotation))
        lastEventTime = System.currentTimeMillis()
    }

    /**
     * this function gets called when a mouse button is pressed
     * it adds a MousePressedEvent to the eventList
     *
     * @see MousePressedEvent
     */
    override fun nativeMousePressed(event: NativeMouseEvent?) {
        if(event == null) return
        eventList.add(MousePressedEvent(if(eventList.size == 0) 0 else System.currentTimeMillis() - lastEventTime, event.button))
        lastEventTime = System.currentTimeMillis()
        //        println("Mouse Button Pressed: Button ${event.button}")
    }

    /**
     * this function gets called when the mouse is moved
     * it adds a MouseMovedEvent to the eventList
     *
     * @see MouseMovedEvent
     */
    override fun nativeMouseMoved(event: NativeMouseEvent?) {
        if(event == null) return
        vb = if(Server.clientInFocus == Server.server) Screen.getPrimary().bounds else Screen.getPrimary().visualBounds

        x = (event.x - vb.minX) / vb.width
        y = (event.y - vb.minY) / vb.height

        eventList.add(MouseMovedEvent(if(eventList.size == 0) 0 else System.currentTimeMillis() - lastEventTime, limit(x), limit(y)))

        lastEventTime = System.currentTimeMillis()
        if(x > 0 && y > 0 && x < 1 && y < 1) {
        }
        else if(consumeWhenBad) {

            val f = NativeInputEvent::class.java.getDeclaredField("reserved")
            f.isAccessible = true
            f.setShort(event, 0x01.toShort())
        }
        else {
            //            println("linux bad")
        }
    }

    /**
     * this function gets called when the mouse is clicked
     * it doesn't really help us, not implemented
     */
    override fun nativeMouseClicked(event: NativeMouseEvent?) {}

    /**
     * this function gets called when a mouse button is released
     * it adds a MouseReleasedEvent to the eventList
     *
     * @see MouseReleasedEvent
     */
    override fun nativeMouseReleased(event: NativeMouseEvent?) {
        if(event == null) return

        eventList.add(MouseReleasedEvent(if(eventList.size == 0) 0 else System.currentTimeMillis() - lastEventTime, event.button))
        lastEventTime = System.currentTimeMillis()
    }

    /**
     * this function gets called when the mouse is dragged
     * since we don't care if the mouse is dragged or just moved, it just adds a MouseMovedEvent to the eventList
     *
     * @see nativeMouseMoved
     * @see MouseMovedEvent
     */
    override fun nativeMouseDragged(event: NativeMouseEvent?) {
        /*if(event == null) return

        eventList.add(MouseMovedEvent(if(eventList.size == 0) 0 else System.currentTimeMillis() - lastEventTime, event.x.d / Screen.getPrimary().visualBounds.width, event.y.d / Screen.getPrimary().visualBounds.height))

        lastEventTime = System.currentTimeMillis()*/
        nativeMouseMoved(event)
    }

    private fun limit(d: Double): Double {
        return max(0.0, min(1.0, d))
    }
}