package org.multex

import javafx.application.Platform
import javafx.scene.Scene
import javafx.stage.Stage
import org.multex.ui.Styles
import org.multex.ui.views.FileTransferView
import org.multex.ui.views.StartView
import tornadofx.*
import tornadofx.App

fun main(args: Array<String>) {
    launch<org.multex.App>()
}

class App: App(StartView::class, Styles::class) {
    private val newStage = Stage()

    override fun start(stage: Stage) {
        super.start(stage)

        trayicon(resources.stream("/trayicon.png"), "Multex") {
            setOnMouseClicked(fxThread = true) {
                FX.primaryStage.show()
                FX.primaryStage.toFront()
            }

            menu("Multex") {
                item("Show") {
                    setOnAction(fxThread = true) {
                        FX.primaryStage.show()
                        FX.primaryStage.toFront()
                    }
                }

                item("send files..") {
                    setOnAction(fxThread = true) {
                        try {
                            val newScene = Scene(find(FileTransferView::class, scope).root)
                            FX.applyStylesheetsTo(newScene)

                            newStage.title = "File Transfer"
                            newStage.scene = newScene
                        }
                        catch(e: Exception) {
                        }
                        finally {
                            newStage.show()
                            newStage.toFront()
                        }
                    }
                }

                item("Exit") {
                    setOnAction(fxThread = true) {
                        Platform.exit()
                        System.exit(0)
                    }
                }
            }
        }
    }
}

